#include <iostream>
#include <fstream>
#include "Busqueda.h"

Busqueda::Busqueda(){}

void Busqueda::arregloEncadenamiento(Nodo **arreglo, int *arreglo2, int largo) {
	
	for (int i = 0; i < largo; ++i) {
		arreglo[i] -> dato = arreglo2[i];
		arreglo[i] -> sig = NULL;
	}
}

void Busqueda::searchEncadenamiento(int *arreglo, int largo, int clave, int posicion){
	Nodo *temp = NULL;
	Nodo *arregloAux[largo];

	for (int i = 0; i < largo; ++i) {
		arregloAux[i] = new Nodo();
		arregloAux[i]->sig = NULL;
		arregloAux[i]->dato = '\0';

	}
	arregloEncadenamiento(arregloAux, arreglo, largo);

	if ((arreglo[posicion] != '\0') && (arreglo[posicion] == clave)) {
		cout << " Clave (" << clave << ") en posicion <" << posicion << ">" << endl;
	} else {
		temp = arregloAux[posicion]->sig;
		while ((temp != NULL) && (temp->dato != clave)) {
			temp = temp->sig;
		}

		if (temp == NULL) {
			cout << " Clave <" << clave << "> no encontrada en la Lista" << endl;
		} else {
			cout << " Clave (" << clave << ") en posicion <" << posicion << ">" << endl;
		}
	}
}

void Busqueda::searchDobleHash(int *arreglo, int largo, int clave, int posicion){
		int nuevaPosicion;

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave)) { // Revisa si el número ya esta en el arreglo
		
		cout << " Clave (" << clave << ") en posicion <" << posicion << ">" << endl;
	} else {
		nuevaPosicion = ((posicion + 1)%largo - 1) + 1;
		while ((nuevaPosicion <= largo) && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != clave)) {
			//Resta uno al tamaño porque el arreglo parte desde cero
			nuevaPosicion = ((nuevaPosicion + 1)%largo -1) + 1;
		}
		if ( (arreglo[nuevaPosicion] == '\0') || (nuevaPosicion == posicion) ) {
			cout << " Clave <" << clave << "> no encontrado" << endl;
		} else {
			cout << " Clave (" << clave << ") en posicion <" << nuevaPosicion << ">" << endl;
		}
	}
}

void Busqueda::ingresoDobleHash(int *arreglo, int largo, int clave, int posicion){
	int nuevaPosicion;

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave) ) {
		// Revisa si el número ya existe en el arreglo 
		cout << " Clave (" << clave << ") en posicion <" << posicion << ">" << endl;
	} else {
		//Se le resta uno al largo porque el arreglo parte desde cero
		nuevaPosicion = ((posicion + 1)%largo - 1) + 1;

		while ((nuevaPosicion <= largo) && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != clave)){
			nuevaPosicion = ((nuevaPosicion + 1)%largo - 1) + 1;
		}

		if (arreglo[nuevaPosicion] == '\0' or (arreglo[nuevaPosicion] != clave) ) {
			// Revisa si la nueva posición esta vacia
			arreglo[nuevaPosicion] = clave;
			if(nuevaPosicion == largo + 1){
				cout << " EL arreglo está Lleno" << endl;
			} else { 
				cout << " Clave (" << clave << ") desplazada a la posición <" << nuevaPosicion << ">" << endl;
			}

		} else { 
				cout << " Clave (" << clave << ") en posicion <" << nuevaPosicion << ">" << endl;
		}
	}
}

void Busqueda::searchCuadratica(int *arreglo, int largo, int clave, int posicion){
	int nuevaPosicion;
	int i;

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave)) {
		// Revisa si el número ya existe en el arreglo 
		cout << "Clave (" << clave << ") en la posicion <" << posicion << ">" << endl;
	} else {
		i = 1;
		// Avanza a la nueva posición con un incremente al cuadrado
		nuevaPosicion = (posicion + (i*i));
		while ( (arreglo[nuevaPosicion] != '\0') && (arreglo[nuevaPosicion] != clave)) {
			//Revisa si la nueva posición esta vacia
			i = i + 1; //Avanza una posición
			nuevaPosicion = (posicion + (i*i)); //acá se genera una nueva posición

			if(nuevaPosicion > largo){ 
				i = 1;
				nuevaPosicion = 0; //Vuelve al inicio del arreglo
				posicion = 0;
			}
		}
		if (arreglo[nuevaPosicion] != '\0')	{
			cout << " Clave <" << clave << "> no encontrado" << endl;
		} else {
			cout << " Clave (" << clave << ") en posicion <" << nuevaPosicion << ">" << endl;
		}
	}
}

void Busqueda::ingresoCuadratica(int *arreglo, int largo, int clave, int posicion){
	int nuevaPosicion;
	int i; // Variable que aumentara cuadráticamente

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave) ) {
		cout << "Clave (" << clave << ") en la posicion <" << posicion << ">" << endl;
	} else {
		i = 0;
		// Avanza a la nueva posición con un incremento al cuadrado
		nuevaPosicion = (posicion + (i*i));

		while( (arreglo[nuevaPosicion] != '\0') && (arreglo[nuevaPosicion] != clave) ) { 
			//Revisa si la nueva posición esta vacia
			
			i++; //Aumenta para avanzar una posición
			nuevaPosicion = (posicion + (i*i)); //Genera una nueva posición

			if (nuevaPosicion > largo) {
				i = 0;
				nuevaPosicion = 0; //Vuelve al inicio del arreglo
				posicion = 0;
			}
		}

		if (arreglo[nuevaPosicion] == '\0') {
			
			arreglo[nuevaPosicion] = clave;
			cout << " Clave (" << clave << ") desplazada a la posición <" << nuevaPosicion << ">" << endl;
		} else {
			cout << " Clave (" << clave << ") en posicion <" << nuevaPosicion << ">" << endl;
		}
	}
}

void Busqueda::searchLineal(int *arreglo, int largo, int clave, int posicion){
	int nuevaPosicion;

	if ((arreglo[posicion] != '\0') && (arreglo[posicion] == clave) ) {
		cout << " Clave (" << clave << ") en posicion <" << posicion << ">" << endl;
	} else {
		nuevaPosicion = posicion + 1;
		while ((arreglo[nuevaPosicion] != '\0') && (nuevaPosicion <= largo) && (nuevaPosicion != posicion) && (arreglo[nuevaPosicion] != clave)){
			nuevaPosicion = nuevaPosicion + 1;
			if (nuevaPosicion == largo + 1) {
				nuevaPosicion = 0; //Vuelve al inicio
			}
		}
		if( (arreglo[nuevaPosicion] == '\0') or (nuevaPosicion == posicion)) {
			cout << " Clave <" << clave << "> no encontrado" << endl;
		} else {
			cout << " Clave (" << clave << ") en posicion <" << nuevaPosicion << ">" << endl;
		}
	}
}

void Busqueda::ingresoLineal(int *arreglo, int largo, int clave, int posicion){
	int nuevaPosicion;
	int cont;

	if ( (arreglo[posicion] != '\0') && (arreglo[posicion] == clave)) {
		// Revisa si el número ya existe en el arreglo 
		cout << " Clave en la posicion <" << posicion << ">" << endl;
	} else {
		cont = 0;
		nuevaPosicion = posicion + 1;

		while ( (nuevaPosicion != posicion)  && (arreglo[nuevaPosicion] != '\0') && (nuevaPosicion <= largo) && (arreglo[nuevaPosicion] != clave) ){
			nuevaPosicion = nuevaPosicion + 1;
			if (nuevaPosicion == (largo + 1)) {
				nuevaPosicion = 0; // Vuelve al inicio
			}
			cont++;
		}

		if (arreglo[nuevaPosicion] == '\0')	{
			// Posicion libre 
			arreglo[nuevaPosicion] = clave;
			cout << " Clave desplazada a la posición <" << ">" << endl;
		}

		if (cont == largo) {
			cout << " Arreglo Lleno" << endl;
		}
	}
}

int Busqueda::hash(int clave, int largo) {
	// Función hash por módulo, asigna una posición para guardar el dato
	clave = (clave%(largo - 1)) + 1;
	return clave;
}
