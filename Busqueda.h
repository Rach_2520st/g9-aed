#include <iostream>
using namespace std;

#ifndef BUSQUEDA_H
#define BUSQUEDA_H

//Estructura del nodo
typedef struct _Nodo{
	int dato;
	struct _Nodo *sig;
} Nodo;

class Busqueda 
{
	public:
		// Constructor 
		Busqueda();
		~Busqueda();

		//métodos que realizan los ingresos
		int hash(int clave, int largo);
		void ingresoLineal(int *arreglo, int largo, int clave, int posicion);
		void ingresoCuadratica(int *arreglo, int largo, int clave, int posicion);
		void ingresoDobleHash(int *arreglo, int largo, int clave, int posicion);
		void arregloEncadenamiento(Nodo **arreglo, int *arreglo2, int largo);

		//métodos que realizan las búsquedas
		void searchLineal(int *arreglo, int largo, int clave, int posicion);
		void searchCuadratica(int *arreglo, int largo, int clave, int posicion);
		void searchDobleHash(int *arreglo, int largo, int clave, int posicion);
		void searchEncadenamiento(int *arreglo, int largo, int clave, int posicion);
	private:
	
};
#endif
