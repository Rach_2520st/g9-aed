

Obtención del Programa

Para su otención, debe clonar repositorio y ejecutar mediante la terminal el comando "make" y luego el comando "./programa argumento ", argumento corresponde al método de resolución de colisiones.


Acerca de:

Para iniciar el programa es necessario el ingreso de un argumento y el comando para la ejecución del programa. En caso de no ingresar el argumento se notificará al usuario que reinicie el programa. El  parametro ingresado indica el metodo de colisiones que desea usar, ya sea en mayuscula o en minuscula.

Una vez realizadas estas verificaciones, se informará al usuario el metodo selesccionado y se solicitará el ingreso del largo del arreglo que se creará. Luego, se mostrará un menú con 3 opciones.

La primera opcion permite el ingreso de numeros al arreglo recientemente creado, se informará al usuario en caso de que ocurran colisiones y donde. La segunda alternativa corresponde a la busqueda de la informacion ingresada de acuerdo al metodo seleccionado. Finalmente la tercera opcion permite la salida o cierre del programa.


Requisitos:

- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)


Autor:

Rachell Aravena Martínez.
