#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Busqueda.h"

using namespace std;

//Imprime arreglo
void imprimir(int arreglo[], int largo) {
	cout << "   [Arreglo]-> {";
	for (int i = 0; i < largo; ++i) {
		/* code */
		if (arreglo[i] == '\0') {
			cout << " * ";
		} else {
			cout << " " << arreglo[i] << " ";
		}
	}
	cout << "}" << endl;
}


// Comprueba el metodo para buscar dato en caso de colision */
void search(int *arreglo, int largo, int dato, string metodo, int posicion) {
	Busqueda *hash = new Busqueda();

	if (metodo == "L" or metodo == "l") {
		cout << " METODO SELECCIONADO: Lineal" << endl;
		hash->searchLineal(arreglo, largo, dato, posicion);

	} else if (metodo == "C" or metodo == "c") {
		cout << " METODO SELECCIONADO: Cuadrática" << endl;
		hash->searchCuadratica(arreglo, largo, dato, posicion);

	} else if (metodo == "D" or metodo == "d") {
		cout << " METODO SELECCIONADO: Doble Dirección Hash" << endl;
		hash->searchDobleHash(arreglo, largo, dato, posicion);

	}else if (metodo == "E" or metodo == "e") {
		cout << " METODO SELECCIONADO: Encadenamiento" << endl;
		hash->searchEncadenamiento(arreglo, largo, dato, posicion);
	}
	imprimir(arreglo, largo);
}


void ingreso(int *arreglo, int largo, int dato, string metodo, int posicion) {
	
	Busqueda *hash = new Busqueda();

	if (metodo == "L" or metodo == "l") {
		cout << " Ha seleccionado el método Lineal" << endl;
		hash->ingresoLineal(arreglo, largo, dato, posicion);

	} else if (metodo == "C" or metodo == "c") {
		cout << " Ha seleccionado el método Cuadrática" << endl;
		hash->ingresoCuadratica(arreglo, largo, dato, posicion);

	} else if (metodo == "D" or metodo == "d") {
		cout << " Ha seleccionado el método Doble Dirección Hash" << endl;
		hash->ingresoDobleHash(arreglo, largo, dato, posicion);

	}else if (metodo == "E" or metodo == "e") {
		cout << " Ha seleccionado el método Encadenamiento" << endl;
		hash->searchEncadenamiento(arreglo, largo, dato, posicion);
	}
	imprimir(arreglo, largo);
}

//Revisa el estado del arreglo, para ver si se pueden agregar numeros
bool estadoArreglo(int arreglo[], int largo) {
	int cont = 0;
	for (int i = 0; i < largo; ++i) { //recorrer arreglo
		if (arreglo[i] == '\0') {
			
			cont++;
		}
	}
	if (cont == largo) {
		//arreglo vacio
		return true;
	} else {
		//arreglo no esta vacio, lleno o con valores agregados
		return false;
	}
}

void arregloVacio(int *arreglo, int largo) {
	// Llenar arreglo de ceros
	for (int i = 0; i < largo; ++i) {
		arreglo[i] = '\0';
	}
}

void menu(string metodo, int *arreglo, int largo){
	
	int opcion;
	int dato;
	Busqueda *hash = new Busqueda();
	int posicion;

	do {
		cout << "*********MENU*********" << endl;
		cout << " 1. Ingresar Número" << endl;
		cout << " 2. Buscar Número" << endl;
		cout << " 3. Salir" << endl;
		cout << "Ingrese su opción: ";
		cin >> opcion;

		switch(opcion) {
			case 1:
				cout << "*********INSERTAR*********" << endl;
				
				cout << " Ingrese un número: ";
				cin >> dato;
				//Obtencion de posicion con Hash
				posicion = hash->hash(dato, largo);
				
				if (arreglo[posicion] == '\0') {
				 // si la posicion esta vacia, se agrega el dato
					arreglo[posicion] = dato;
					imprimir(arreglo, largo);
				} else {
					//si la posicion está ocupada, se produce colision 
					cout << " Colisión en la posicion <";
					cout << posicion << ">" << endl;
					ingreso(arreglo, largo, dato, metodo, posicion);
				}
				
				break;

			case 2:
				cout << "***********BUSQUEDA************" << endl;
				cout << "  Ingrese el número que busca: ";
				cin >> dato;

				if (estadoArreglo(arreglo, largo) == false) {
					//arreglo no esta vacio, lleno o con numeros
					//Obtener posicion
					posicion = hash->hash(dato, largo);
					if (arreglo[posicion] == dato) {
						cout << " Clave (" << dato << ") en posicion <";
						cout << posicion << ">" << endl;
						imprimir(arreglo, largo);
					} else {
						cout << " Colisión en la posicion <";
						cout << posicion << ">" << endl;
						//Revisa con que método se resolverá la colisión
						search(arreglo, largo, dato, metodo, posicion);
					}
				} else {
					
					cout << "ESTADO: Arreglo vacio" << endl;
					cout << " Elemento <" << dato << "> no encontrado" << endl;
				}
				cout << "\n";
				break;

			case 3:
				exit(1);
				break;

			default:
				cout << " Opcion no valida" << endl;
				menu(metodo, arreglo, largo);
				break;
		}
	} while (opcion != 3);
}

int main(int argc, char *argv[]) {
	// Validar ingreso de los 2 argumentos 
	if (argc != 2) {
		// no ingreso los 2 argumentos 
		cout << "ERROR. Número argumentos inválido" << endl;
		cout << " Ingrese el método de resolución de colisiones" << endl;
		cout << " Reinicie el programa" << endl;
		exit(1);

	} else { //ingresó 2 argumentos

		// Comprobar que el segundo sea un entero 
		try {
			string metodo = argv[1]; //segundo argumento
			
			if (metodo == "L" or metodo == "l") {
				cout << "El metodo que seleccionó es Lineal" << endl;

			} else if (metodo == "C" or metodo == "c") {
				cout << "El metodo que seleccionó es Cuadrática" << endl;

			} else if (metodo == "D" or metodo == "d") {
				cout << "El metodo que seleccionó es Doble Dirección Hash" << endl;

			}else if (metodo == "E" or metodo == "e") {
				cout << "El metodo que seleccionó es Encadenamiento" << endl;

			} else {
				// Argumento invalido
				cout << "ERROR. Segundo argumento inválido" << endl;
				cout << " Ingrese la letra correspondiente al método en Mayúscula";
				cout << "\n Opciones posibles: {L|C|D|E}" << endl;
				cout << " Reinicie el programa" << endl;
				exit(1);
			}

			int largo;
			cout << " > Ingrese el tamaño del arreglo: ";
			cin >> largo;
			int arreglo[largo]; //Inicializa el  arreglo
			arregloVacio(arreglo, largo); //Llenar arreglo de ceros para identificarlo
			imprimir(arreglo, largo);

			//muestra el menú
			menu(metodo, arreglo, largo);

		} catch (const std::invalid_argument& e) {
			// segundo argumento no es una Letra
			cout << "ERROR: Segundo argumento inválido" << endl;
			cout << " Segundo argumento debe ser de tipo char {L|C|D|E}" << endl;
			cout << " Reinicie el programa" << endl;
			exit(1);
		}
	}
	return 0;
}
